# Implementación de la arquitectura de un chat
8CC2 Cloud Computing

07 de abril 2022

Maritrini Velázquez Ruiz 329675

## Requisitos
1) Imagen de docker de rabbit mq
```bash
docker run -dti -P —hostname=rabbit —name=rabbit -e RABBITMQ_DEFAULT_USER=user -e RABBITMQ_DEFAULT_PASS=password rabbitmq:3-management
```
2) Node JS

## git clone
```bash
git clone git@gitlab.com:Maritrini00/chat-con-rabbitmq.git
```
Instalar todos los paquetes que se utilizan en la aplicación
```bash
npm install
```

## ¿Cómo correr la aplicación?
```bash
node friends.js
```
## License
la UACH
