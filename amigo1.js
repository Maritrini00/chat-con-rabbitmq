const amqp = require('amqplib/callback_api');
const readline = require('readline');

// amqp://<user>?:?<password>?@?<host>:<port>
amqp.connect("amqp://maritrini:abcd1234@localhost:49158",(err, con)=>{

    if(err){
        throw err;
    }

    con.createChannel((err1,channel)=>{
        if(err1){
            throw err1;
        }

        let message = 'hola'; //readline.createInterface(process.stdin,process.stdout);
        let queue = "mensajesMaritrini";
        let queue2= "mensajesMaritrini2";

        /*message.setPrompt(`Mensaje: `);
        message.prompt();
        message.on('line',(msg)=>{
        console.log('envaindo mensaje');
        message.close();*/
    

        channel.assertQueue(queue,{
            durable: false
        });

        channel.sendToQueue(queue,Buffer.from(message));
        console.log("mensaje enviado.. esperar respuesta");

        channel.consume(queue2,(message2)=>{
            console.log("-> "+message2.content.toString());
        },{noAck: true});

    });

    setTimeout(()=>{
        con.close();
        process.exit(0);
    },500);

});