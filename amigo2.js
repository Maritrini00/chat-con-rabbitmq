const amqp = require('amqplib/callback_api');

amqp.connect("amqp://maritrini:abcd1234@localhost:49158",(err, con)=>{

    if(err){
        throw err;
    }

    con.createChannel((err1,channel)=>{

        if(err1){
            throw err1;
        }

        let queue = "mensajesMaritrini";
        channel.assertQueue(queue,{
            durable: false
        });

        console.log("Esperando mensaje")

        channel.consume(queue,(message)=>{
            console.log("-> "+message.content.toString());
        },{noAck: true});

        let message2 = 'holax2'; //readline.createInterface(process.stdin,process.stdout);
        let queue2 = "mensajesMaritrini2";

        channel.assertQueue(queue2,{
            durable: false
        });

        channel.sendToQueue(queue2,Buffer.from(message2));
        console.log("mensaje enviado.. esperar respuesta");


        
    });
    setTimeout(()=>{
        con.close();
        process.exit(0);
    },500);

});